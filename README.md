# React on Rails API

This application is a sorta blog made with React and a rails API.
CRUD features available !
React is used for the client, that make some requests using Axios to the rails API.

* Ruby version : 2.5.1
* Rails Version : 5.2.1
* Foreman
* React
* PostGreSQL
* Axios


Launch the app :
    rake start
url : localhost:3000