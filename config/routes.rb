Rails.application.routes.draw do
  resources :list_items
  namespace :api do
    namespace :v1 do
      resources :items
      resources :lists    
    end
  end
end
